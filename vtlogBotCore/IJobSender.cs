﻿namespace vtlogBotCore;
internal interface ICommunicator
{
    Task SendJob(IEmbeddable job);
    Task SendText(string text);
    Task SendAdminText(string text);
}
