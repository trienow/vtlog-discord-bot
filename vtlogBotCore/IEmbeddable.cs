﻿using Discord;

namespace vtlogBotCore;
internal interface IEmbeddable
{
    Embed CreateEmbed();
}
