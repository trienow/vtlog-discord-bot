﻿using Config.Net;

namespace vtlogBotCore.Configuration;
public interface IVtlog
{
    [Option(DefaultValue = "vtlog.net")]
    public string Hostname { get; set; }
    public string CompanyId { get; set; }
    public string ApiToken { get; set; }
}
