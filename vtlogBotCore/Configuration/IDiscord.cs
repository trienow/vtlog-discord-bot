﻿namespace vtlogBotCore.Configuration;
public interface IDiscord
{
    public string BotToken { get; set; }
    public ulong? ChannelId { get; set; }
    public string BotAdministrator { get; set; }
}
