﻿namespace vtlogBotCore.Configuration;
public interface IApp
{
    public bool? LogVerbose { get; set; }
}
