﻿namespace vtlogBotCore.Configuration;
public interface IReplacements
{
    IEnumerable<IReplacement> CargoNames { get; }
    IEnumerable<IReplacement> PlaceNames { get; }
}
