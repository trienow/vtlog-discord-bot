﻿namespace vtlogBotCore.Configuration;
public interface IReplacement
{
    string From { get; }
    string To { get; }
}
