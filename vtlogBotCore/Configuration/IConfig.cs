﻿namespace vtlogBotCore.Configuration;
public interface IConfig
{
    IApp App { get; }
    IVtlog Vtlog { get; }
    IDiscord Discord { get; }
}
