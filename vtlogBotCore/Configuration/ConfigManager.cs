﻿using Config.Net;

namespace vtlogBotCore.Configuration;
public static class ConfigManager
{
    private static IConfig? cfg;
    private static IAppState? _appState;

    public static IAppState AppState { get => _appState ?? LoadAppState(); }

    public static IApp App { get => (cfg ?? LoadConfig()).App; }
    public static IVtlog Vtlog { get => (cfg ?? LoadConfig()).Vtlog; }

    public static IDiscord Discord { get => (cfg ?? LoadConfig()).Discord; }

    public static IReplacements LoadReplacements()
    {
        IReplacements replacements = new ConfigurationBuilder<IReplacements>()
            .UseJsonFile("./botConfig/replacements.json")
            .Build();
        return replacements;
    }

    private static IAppState LoadAppState()
    {
        _appState = new ConfigurationBuilder<IAppState>()
            .UseJsonFile("./botConfig/appState.json")
            .Build();
        return _appState;
    }

    public static IConfig LoadConfig()
    {
        cfg = new ConfigurationBuilder<IConfig>()
            .UseJsonFile("./botConfig/config.json")
            .UseCommandLineArgs()
            .Build();

        SetVtlogDefaults(cfg.Vtlog);
        SetDiscordDefaults(cfg.Discord);
        SetAppDefaults(cfg.App);

        return cfg;
    }

    private static void SetAppDefaults(IApp app)
    {
        app.LogVerbose ??= false;
    }

    private static void SetVtlogDefaults(IVtlog vtlog)
    {
        if (string.IsNullOrEmpty(vtlog.Hostname))
        {
            vtlog.Hostname = "https://vtlog.net";
        }

        if (string.IsNullOrEmpty(vtlog.CompanyId))
        {
            vtlog.CompanyId = "5662";
        }
    }

    private static void SetDiscordDefaults(IDiscord discord)
    {
        discord.BotToken ??= string.Empty;
        discord.ChannelId ??= 0;
        discord.BotAdministrator ??= string.Empty;
    }
}
