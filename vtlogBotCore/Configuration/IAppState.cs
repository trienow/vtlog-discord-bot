﻿using Config.Net;

namespace vtlogBotCore.Configuration;
public interface IAppState
{
    [Option(DefaultValue = -1L)]
    long NewestJob { get; set; }
}
