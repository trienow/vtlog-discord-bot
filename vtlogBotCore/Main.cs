﻿using vtlogBotCore.Discord;
using vtlogBotCore.VtLog;

namespace vtlogBotCore;
public class Main
{
    readonly CancellationTokenSource cancelSource = new CancellationTokenSource();

    public void Run()
    {
        Logger.Log.Information($"Hello!");

        MainDiscord discord = new MainDiscord();
        MainVtLog vtlog = new MainVtLog(discord);

        discord.BotOnline += (s, e) =>
        {
            if (!vtlog.IsRunning)
            {
                Task.Factory.StartNew(async () =>
                {
                    if (!await vtlog.Run(cancelSource.Token))
                    {
                        cancelSource.Cancel();
                    }
                });
            }
        };

        Task.Factory.StartNew(async () =>
        {
            await discord.Run(cancelSource.Token);
        });

        bool mayRun = true;

        while (mayRun)
        {
            if (Console.ReadLine() == "exit")
            {
                cancelSource.Cancel();
                mayRun = false;
            }
        }

        Console.WriteLine("App exited.");
        Console.Read();
    }
}
