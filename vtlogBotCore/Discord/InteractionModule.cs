﻿using Discord.Interactions;
using vtlogBotCore.Configuration;
using static vtlogBotCore.Logger;

namespace vtlogBotCore.Discord;
public class InteractionModule : InteractionModuleBase<SocketInteractionContext>
{
    [SlashCommand("vtbot-reload-config", "Reloads the bots config")]
    public async Task ReloadConfig()
    {
        Log.Information($"{Context.User.Username} is reloading the config.");
        ConfigManager.LoadConfig();
        await RespondAsync("Config reloaded.");
    }
}
