﻿using Discord;
using Discord.Interactions;
using Discord.WebSocket;

namespace vtlogBotCore.Discord;
internal class MainDiscord : ICommunicator
{
    private static Configuration.IDiscord Cfg { get => Configuration.ConfigManager.Discord; }
    private readonly DiscordSocketClient client;
    private readonly RequestOptions defaultRequestOptions = new RequestOptions();
    private bool gatewayWarningIsVerbose = false;

    private IMessageChannel? _messageChannel = null;
    private InteractionService? interactionService;

    public event EventHandler? BotOnline;

    public MainDiscord()
    {
        DiscordSocketConfig socketConfig = new DiscordSocketConfig
        {
            GatewayIntents = GatewayIntents.Guilds | GatewayIntents.GuildMessages
        };

        client = new DiscordSocketClient(socketConfig);
        client.Log += Log;
    }

    internal async Task Run(CancellationToken token)
    {
        defaultRequestOptions.CancelToken = token;
        defaultRequestOptions.Timeout = 30_000;

        client.Ready += Client_Ready;

        await client.LoginAsync(TokenType.Bot, Cfg.BotToken);
        await client.StartAsync();

        try
        {
            await Task.Delay(-1, token);
        }
        catch (TaskCanceledException) { }

        await client.StopAsync();
        await client.LogoutAsync();
    }

    private async Task Client_Ready()
    {
        LogInfo("Bot setting up commands.");

        interactionService = new InteractionService(client);

        ModuleInfo info = await interactionService.AddModuleAsync<InteractionModule>(null);
        await interactionService.RegisterCommandsGloballyAsync(true);
        client.InteractionCreated += Client_InteractionCreated;

        LogInfo("Bot ready.");
        BotOnline?.Invoke(this, EventArgs.Empty);
    }

    private async Task Client_InteractionCreated(SocketInteraction arg)
    {
        SocketInteractionContext ctx = new SocketInteractionContext(client, arg);
        await interactionService!.ExecuteCommandAsync(ctx, null);
    }

    public async Task SendJob(IEmbeddable embeddable)
    {
        try
        {
            IMessageChannel mc = await GetMessageChannel();
            await mc.SendMessageAsync(null, embed: embeddable.CreateEmbed(), options: defaultRequestOptions, allowedMentions: new AllowedMentions(AllowedMentionTypes.None));
        }
        catch (TaskCanceledException) { }
    }

    public async Task SendAdminText(string text)
    {
        if (!string.IsNullOrEmpty(Cfg.BotAdministrator))
        {
            text = $"Hey <@{Cfg.BotAdministrator}>! " + text;
        }
        await SendText(text);
    }

    public async Task SendText(string text)
    {
        try
        {
            IMessageChannel mc = await GetMessageChannel();
            await mc.SendMessageAsync(text);
        }
        catch (TaskCanceledException) { }
    }

    private async Task<IMessageChannel> GetMessageChannel()
    {
        return _messageChannel ??= (IMessageChannel)await client.GetChannelAsync(Cfg.ChannelId ?? 0);
    }

    private Task Log(LogMessage msg)
    {
        string message = $"Discord.NET: {msg.ToString(prependTimestamp: false)}";

        if (msg.Source == "Gateway" && msg.Message != null)
        {
            if (msg.Message.Contains("Connected") || msg.Message.Contains("Resumed previous session"))
            {
                gatewayWarningIsVerbose = true;
            }
        }

        switch (msg.Severity)
        {
            case LogSeverity.Critical:
                Logger.Log.Fatal(message);
                break;
            case LogSeverity.Error:
                Logger.Log.Error(message);
                break;
            case LogSeverity.Warning:
                if (msg.Source == "Gateway")
                {
                    if (gatewayWarningIsVerbose)
                    {
                        Logger.Log.Verbose(message);
                        gatewayWarningIsVerbose = false; //Reset, as more warnings might be real warnings.
                    }
                    else
                    {
                        Logger.Log.Warning(message);
                    }
                }
                else
                {
                    Logger.Log.Warning(message);
                }
                break;
            case LogSeverity.Info:
                Logger.Log.Information(message);
                break;
            case LogSeverity.Verbose:
                Logger.Log.Verbose(message);
                break;
            case LogSeverity.Debug:
                Logger.Log.Debug(message);
                break;
        }
        return Task.CompletedTask;
    }

    private static void LogInfo(object msg)
    {
        Logger.Log.Information($"Discord: {msg}");
    }
}
