﻿using Serilog;
using Serilog.Events;
using System.Reflection;

namespace vtlogBotCore;
public static class Logger
{
    private static ILogger? log;
    public static ILogger Log { get => log ?? BuildLogger(); }

    private static ILogger BuildLogger()
    {
        const string outputTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff} [{Level:u3}]{SourceContext} {Message:lj}{NewLine}{Exception}"; //Message:lj is default
        string exeDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) ?? string.Empty;
        string basePath = Path.Combine(exeDirectory, "logs") + "\\";

        bool logVerbose = Configuration.ConfigManager.App.LogVerbose ?? false;
        LoggerConfiguration logCfg = new LoggerConfiguration();

        if (logVerbose)
        {
            logCfg.MinimumLevel.Verbose()
            .WriteTo.File($"{basePath}vrbs.log", restrictedToMinimumLevel: LogEventLevel.Verbose, outputTemplate: outputTemplate, fileSizeLimitBytes: 100_000_000, buffered: true, rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true, retainedFileCountLimit: 20, flushToDiskInterval: new TimeSpan(0, 1, 0));
        }

        log = logCfg.WriteTo.Console()
        .WriteTo.File($"{basePath}info.log", restrictedToMinimumLevel: LogEventLevel.Information, outputTemplate: outputTemplate, fileSizeLimitBytes: 50_000_000, buffered: false, rollingInterval: RollingInterval.Month, rollOnFileSizeLimit: true, retainedFileCountLimit: 20)
        .WriteTo.File($"{basePath}warn.log", restrictedToMinimumLevel: LogEventLevel.Warning, outputTemplate: outputTemplate, fileSizeLimitBytes: 1_000_000, buffered: false, rollingInterval: RollingInterval.Month, rollOnFileSizeLimit: true, retainedFileCountLimit: 10)
        .CreateLogger();

        return log;
    }
}
