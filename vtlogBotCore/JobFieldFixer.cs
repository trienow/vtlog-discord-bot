﻿using System.Text;
using System.Text.RegularExpressions;
using vtlogBotCore.Configuration;

namespace vtlogBotCore;
internal class JobFieldFixer
{
    private readonly IReplacements replacements = ConfigManager.LoadReplacements();

    private static string FixName(string? name)
    {
        string fixedName = string.Empty;

        if (name is not null && name.Length > 0)
        {
            StringBuilder builder = new StringBuilder(name);

            builder[0] = char.ToUpper(builder[0]);

            MatchCollection underScores = Regex.Matches(name, @"_.");
            foreach (Match match in underScores.Cast<Match>())
            {
                builder[match.Index] = ' ';
                builder[match.Index + 1] = char.ToUpper(builder[match.Index + 1]);
            }

            fixedName = builder.ToString();
        }

        return fixedName;
    }


    internal string FixPlaceName(string? name)
    {
        bool fixedName = false;
        name ??= string.Empty;
        foreach (IReplacement replacement in replacements.PlaceNames)
        {
            if (Regex.IsMatch(name, replacement.From))
            {
                name = Regex.Replace(name, replacement.From, replacement.To);
                fixedName = true;
                break;
            }
        }

        if (!fixedName)
        {
            name = FixName(name);
        }

        return name;
    }


    internal string FixCargoName(string? name)
    {
        bool fixedName = false;
        name ??= string.Empty;
        foreach (IReplacement replacement in replacements.CargoNames)
        {
            if (Regex.IsMatch(name, replacement.From))
            {
                name = Regex.Replace(name, replacement.From, replacement.To);
                fixedName = true;
                break;
            }
        }

        if (!fixedName)
        {
            name = FixName(name);
        }

        return name;
    }
}
