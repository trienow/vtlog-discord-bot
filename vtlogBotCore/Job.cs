﻿using Discord;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace vtlogBotCore;
public class Job : IEmbeddable
{
    private static readonly NumberStyles VTLOG_NUMBER_STYLE = NumberStyles.Number;

    public long JobId { get; set; }
    public DateTime Date { get; set; }
    public string MemberDiscord { get; set; } = string.Empty;
    public string MemberName { get; set; } = string.Empty;
    public string Route { get; set; } = string.Empty;
    public int DistanceKm { get; set; }
    public string FuelEco { get; set; } = string.Empty;
    public string FuelEfficiency { get; set; } = string.Empty;
    public string Cargo { get; set; } = string.Empty;
    public int DamageTruck { get; set; }
    public int DamageTrailer { get; set; }
    public int DamageCargo { get; set; }
    public string LeaderboardName { get; set; } = string.Empty;
    public int Profit { get; set; }

    public Embed CreateEmbed()
    {
        string member = !string.IsNullOrWhiteSpace(MemberDiscord) && MemberDiscord != "0" ? $"<@{MemberDiscord}>" : MemberName;

        List<EmbedFieldBuilder> fields = new List<EmbedFieldBuilder>
        {
            new EmbedFieldBuilder { Name = "📆 Date**        **", Value = $"{Date:yyyy-MM-dd}", IsInline = true },
            new EmbedFieldBuilder { Name = "🏃 Driver**        **", Value = member, IsInline = true },
            new EmbedFieldBuilder { Name = "🗺 Route", Value = Route, IsInline = true},
            new EmbedFieldBuilder { Name = "📏 Distance", Value = $"{DistanceKm} km", IsInline = true },
            new EmbedFieldBuilder { Name = "⛽ Fuel", Value = FuelEco, IsInline = true },
            new EmbedFieldBuilder { Name = "📦 Cargo", Value = Cargo, IsInline = true },
            new EmbedFieldBuilder { Name = "🔥 Damage %", Value = $"{DamageTruck} | {DamageTrailer} | {DamageCargo}", IsInline = true},
            new EmbedFieldBuilder { Name = "🥦 Fuel", Value = FuelEfficiency, IsInline = true },
            new EmbedFieldBuilder { Name = "⭐ Leaderboard", Value = LeaderboardName, IsInline = true },
            new EmbedFieldBuilder { Name = "📈 Profit", Value = Profit, IsInline = true},
        };

        Color color;
        if (Profit > 0)
        {
            if (LeaderboardName == "Realistic")
            {
                color = new Color(62, 223, 62); //Grass Green
            }
            else
            {
                color = new Color(222, 222, 62); //Yellow
            }
            //color = new Color(169, 222, 62); //Lime Green
            //color = new Color(222, 222, 62); //Yellow
            //color = new Color(222, 62, 222); //Magenta
        }
        else if (Profit == 0)
        {
            color = new Color(222, 115, 62); //Orange
        }
        else
        {
            color = new Color(223, 62, 62); //VTLog Red
        }

        EmbedFooterBuilder footer = new EmbedFooterBuilder
        {
            Text = $"Job Id: {JobId}"
        };

        EmbedBuilder builder = new EmbedBuilder
        {
            Title = $"Job Delivered | Result: {Profit}",
            Url = $"https://{Configuration.ConfigManager.Vtlog.Hostname}/job/{JobId}",
            Color = color,
            Fields = fields,
            Footer = footer
        };

        return builder.Build();
    }

    public override string ToString()
    {
        return $"Job #{JobId}, " +
            $"{Date}, " +
            $"By: {MemberName} (aka. {MemberDiscord}), " +
            $"Route: {Route}, " +
            $"Dist: {DistanceKm}, " +
            $"FuelEco: {FuelEco}, " +
            $"Cargo: {Cargo}, " +
            $"DamageCargo: {DamageCargo}, " +
            $"DamageTrailer: {DamageTrailer}, " +
            $"DamageTruck: {DamageTruck}, " +
            $"Leaderboard: {LeaderboardName}, " +
            $"Profit: {Profit}";
    }

    internal static int CalcDamage(params string?[] damages)
    {
        decimal avgDmg = 0;
        int dmgCount = 0;
        foreach (string? damage in damages)
        {
            if (decimal.TryParse(damage, VTLOG_NUMBER_STYLE, CultureInfo.InvariantCulture, out decimal dmg))
            {
                avgDmg = ((dmgCount * avgDmg) / (dmgCount + 1)) + (dmg / (dmgCount + 1));
                dmgCount++;
            }
        }
        return (int)avgDmg;
    }

    internal void CalculateFuelEco(decimal kilometers, decimal fuelUsageLiter, string? kilograms)
    {
        decimal tonnes = decimal.One;
        if (decimal.TryParse(kilograms, VTLOG_NUMBER_STYLE, CultureInfo.InvariantCulture, out decimal kg))
        {
            tonnes = kg / 1000;
        }
        decimal fuelEco = (fuelUsageLiter / kilometers) * 100;
        FuelEco = $"{fuelEco:F2} l/100 km";
        FuelEfficiency = $"{fuelEco / tonnes:F2} (l/100 km)/t";
    }
}
