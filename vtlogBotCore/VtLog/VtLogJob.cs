﻿using Newtonsoft.Json;

namespace vtlogBotCore.VtLog;
internal class VtLogJob
{
    [JsonProperty(PropertyName = "job_id")]
    public long Id { get; set; }
    [JsonProperty(PropertyName = "arrival_year")]
    public int ArrivalYear { get; set; }
    [JsonProperty(PropertyName = "arrival_month")]
    public int ArrivalMonth { get; set; }
    [JsonProperty(PropertyName = "arrival_day")]
    public int ArrivalDay { get; set; }
    [JsonProperty(PropertyName = "steam_id")]
    public string? SteamId { get; set; }
    [JsonProperty(PropertyName = "departure_city_id")]
    public string? DepartureCityId { get; set; }
    [JsonProperty(PropertyName = "arrival_city_id")]
    public string? ArrivalCityId { get; set; }
    [JsonProperty(PropertyName = "distance_client")]
    public int Distance { get; set; }
    [JsonProperty(PropertyName = "fuel_used")]
    public int FuelUsed { get; set; }
    [JsonProperty(PropertyName = "truck_cabin_damage")]
    public string? TruckCabinDamage { get; set; }
    [JsonProperty(PropertyName = "truck_chassis_damage")]
    public string? TruckChassisDamage { get; set; }
    [JsonProperty(PropertyName = "truck_engine_damage")]
    public string? TruckEngineDamage { get; set; }
    [JsonProperty(PropertyName = "truck_transmission_damage")]
    public string? TruckTransmissionDamage { get; set; }
    [JsonProperty(PropertyName = "truck_wheels_damage")]
    public string? TruckWheelsDamage { get; set; }
    [JsonProperty(PropertyName = "trailers_chassis_damage")]
    public string? TrailersChassisDamage { get; set; }
    [JsonProperty(PropertyName = "trailers_wheels_damage")]
    public string? TrailersWheelsDamage { get; set; }
    [JsonProperty(PropertyName = "trailer_cargo_damage")]
    public string? TrailerCargoDamage { get; set; }
    [JsonProperty(PropertyName = "trailers_body_damage")]
    public string? TrailerBodyDamage { get; set; }
    [JsonProperty(PropertyName = "cargo_id")]
    public string? CargoId { get; set; }
    [JsonProperty(PropertyName = "cargo_mass")]
    public string? CargoMass { get; set; }
    [JsonProperty(PropertyName = "profit")]
    public int Profit { get; set; }
    [JsonProperty(PropertyName = "board")]
    public string? Board { get; set; }
}
