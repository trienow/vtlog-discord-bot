﻿using Newtonsoft.Json;
using System.Net;

namespace vtlogBotCore.VtLog;
internal class MainVtLog
{
    private static Configuration.IVtlog Cfg { get => Configuration.ConfigManager.Vtlog; }

    private const int NOTIFICATION_ATTEMPTS = 120;

    private readonly HttpClient client = new HttpClient();
    private readonly Configuration.IAppState appState;
    private readonly ICommunicator jobSender;
    private readonly Dictionary<string, VtLogUser> userCache = new Dictionary<string, VtLogUser>();

    public bool IsRunning { get; private set; }

    public MainVtLog(ICommunicator jobSender)
    {
        appState = Configuration.ConfigManager.AppState;
        client.Timeout = new TimeSpan(0, 0, 30);

        this.jobSender = jobSender;
    }

    public async Task<bool> Run(CancellationToken token)
    {
        LogInfo("Starting VTLOG Polling...");
        IsRunning = true;

        int httpFailures = 0;
        int generalFailures = 0;
        bool notifiedAdmin = false;

        try
        {
            while (!token.IsCancellationRequested)
            {
                try
                {
                    LogInfo($"Checking logbook...");
                    await ProcessLogbook(token);

                    if (httpFailures > 0)
                    {
                        LogInfo("Logbook check done.");
                        if (notifiedAdmin)
                        {
                            await jobSender.SendText("vtlog is reachable again! All missing jobs should have gotten sent to this channel by now. :)\r\nPlease note this bot only reads the first page of the log book.");
                        }
                    }
                    else
                    {
                        LogDebug("Logbook check done.");
                    }

                    httpFailures = 0;
                    generalFailures = 0;
                    notifiedAdmin = false;
                }
                catch (Exception ex) when (ex is HttpRequestException hex)
                {
                    httpFailures++;
                    if (httpFailures >= NOTIFICATION_ATTEMPTS && !notifiedAdmin)
                    {
                        string message = "I don't know if vtlog is just down at the moment or a token expired.\r\n";
                        message += "If the token has expired please re-login into vtlog and put the new token into the config. Then run the /vtbot-reload-config command.";
                        await jobSender.SendAdminText(message);
                        notifiedAdmin = true;
                    }

                    LogError($"Error processing logbook. HTTP Failure #{httpFailures}. Details: {ex}");

                    if (hex.StatusCode == HttpStatusCode.TooManyRequests)
                    {
                        await Task.Delay(240000, token);
                    }
                    await Task.Delay(120000, token);
                }
                catch (Exception ex)
                {
                    LogError($"Error processing logbook. General failure #{generalFailures}. Details: {ex}");
                }

                if (string.IsNullOrEmpty(Cfg.ApiToken))
                {
                    LogInfo($"No API-Token provided, sleeping for 6 minutes to avoid rate limits");
                    await Task.Delay(360000, token);
                }
                else
                {
                    await Task.Delay(60000, token);
                }
            }
        }
        catch (TaskCanceledException) { }
        catch (Exception ex)
        {
            LogFatal(ex);
            IsRunning = false;
            return false;
        }
        IsRunning = false;
        return true;
    }

    private async Task ProcessLogbook(CancellationToken token)
    {
        long newestJobId = appState.NewestJob;
        HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Get, $"https://api.{Cfg.Hostname}/v1/vtc/{Cfg.CompanyId}/jobs?after={newestJobId}");
        if (!string.IsNullOrEmpty(Cfg.ApiToken))
        {
            LogDebug("Using Bearer-Token to authenticate...");
            req.Headers.Add("Authorization", $"Bearer {Cfg.ApiToken}");
        }
        HttpResponseMessage res = await client.SendAsync(req, HttpCompletionOption.ResponseContentRead, token);
        if (res.StatusCode != HttpStatusCode.NotFound) //vtlog sends a 404, if no more jobs can be found.
        {
            res.EnsureSuccessStatusCode();

            string resBody = string.Empty;
            using (StreamReader sr = new StreamReader(res.Content.ReadAsStream(token)))
            {
                resBody = sr.ReadToEnd();
            }

            VtLogJobWrapper vtLogJobs = JsonConvert.DeserializeObject<VtLogJobWrapper>(resBody) ?? throw new NullReferenceException("Converting the JSON from vtLog to a VtLogJobWrapper.");
            JobFieldFixer jobFixer = new JobFieldFixer();

            try
            {
                foreach (VtLogJob extJob in vtLogJobs.Data)
                {
                    if (extJob.SteamId is null)
                    {
                        LogError($"SteamId for Job #{extJob.SteamId} is null. Skipping...");
                        continue;
                    }

                    Job job = new Job
                    {
                        JobId = extJob.Id,
                        Date = new DateTime(extJob.ArrivalYear, extJob.ArrivalMonth, extJob.ArrivalDay),
                        Route = $"{jobFixer.FixPlaceName(extJob.DepartureCityId)} to {jobFixer.FixPlaceName(extJob.ArrivalCityId)}",
                        DistanceKm = extJob.Distance,
                        Cargo = jobFixer.FixCargoName(extJob.CargoId),
                        DamageCargo = Job.CalcDamage(extJob.TrailerCargoDamage),
                        DamageTrailer = Job.CalcDamage(extJob.TrailerBodyDamage, extJob.TrailersChassisDamage, extJob.TrailersWheelsDamage),
                        DamageTruck = Job.CalcDamage(extJob.TruckCabinDamage, extJob.TruckChassisDamage, extJob.TruckEngineDamage, extJob.TruckTransmissionDamage, extJob.TruckWheelsDamage),
                        LeaderboardName = extJob.Board ?? "???",
                        Profit = extJob.Profit,
                    };

                    job.CalculateFuelEco(extJob.Distance, extJob.FuelUsed, extJob.CargoMass);

                    if (userCache.TryGetValue(extJob.SteamId, out VtLogUser? extUserCached))
                    {
                        job.MemberDiscord = extUserCached.DiscordId ?? string.Empty;
                        job.MemberName = extUserCached.Username ?? "???";
                    }
                    else
                    {
                        LogVerbose($"Getting user meta for {extJob.SteamId}.");
                        req = new HttpRequestMessage(HttpMethod.Get, $"https://api.{Cfg.Hostname}/v1/user/{extJob.SteamId}");
                        res = await client.SendAsync(req, HttpCompletionOption.ResponseContentRead, token);
                        res.EnsureSuccessStatusCode();

                        resBody = string.Empty;
                        using (StreamReader sr = new StreamReader(res.Content.ReadAsStream(token)))
                        {
                            resBody = sr.ReadToEnd();
                        }

                        VtLogUser extUser = JsonConvert.DeserializeObject<VtLogUser>(resBody) ?? throw new NullReferenceException($"Converting the JSON from vtLog to a VtLogUser failed for user {extJob.SteamId}.");
                        userCache.Add(extJob.SteamId, extUser);

                        job.MemberDiscord = extUser.DiscordId ?? string.Empty;
                        job.MemberName = extUser.Username ?? "???";
                    }

                    if (job.JobId > newestJobId)
                    {
                        newestJobId = job.JobId;
                    }

                    LogVerbose($"{job}");
                    LogInfo($"Found job #{job.JobId} completed by {job.MemberName} from {job.Route}");

                    //Send the job info to discord now
                    await jobSender.SendJob(job);
                }
            }
            finally
            {
                if (newestJobId > appState.NewestJob)
                {
                    LogInfo($"Saving job #{newestJobId} as newest job id");
                    appState.NewestJob = newestJobId;
                }
            }
        }
    }

    private static void LogVerbose(object msg)
    {
        Logger.Log.Verbose($"VTLOG: {msg}");
    }
    private static void LogDebug(object msg)
    {
        Logger.Log.Debug($"VTLOG: {msg}");
    }
    private static void LogInfo(object msg)
    {
        Logger.Log.Information($"VTLOG: {msg}");
    }
    private static void LogError(object msg)
    {
        Logger.Log.Error($"VTLOG: {msg}");
    }
    private static void LogFatal(object msg)
    {
        Logger.Log.Fatal($"VTLOG: {msg}");
    }
}
