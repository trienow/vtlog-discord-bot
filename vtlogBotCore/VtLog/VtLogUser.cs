﻿using Newtonsoft.Json;

namespace vtlogBotCore.VtLog;
internal class VtLogUser
{
    //[JsonProperty(PropertyName = "steam_id")]
    //public string? SteamId { get; set; }
    [JsonProperty(PropertyName = "username")]
    public string? Username { get; set; }
    [JsonProperty(PropertyName = "discord_id")]
    public string? DiscordId { get; set; }
}