﻿using Newtonsoft.Json;

namespace vtlogBotCore.VtLog;
internal class VtLogJobWrapper
{
    [JsonProperty(PropertyName = "data")]
    public List<VtLogJob> Data { get; set; } = new List<VtLogJob>();
}
